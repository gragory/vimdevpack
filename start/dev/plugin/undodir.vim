set undofile
set undodir=$HOME/.vim/pack/vimdevpack/cache/undo
set undolevels=1000
set undoreload=10000
